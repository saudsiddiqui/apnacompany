<?php

/**
 * The Visitor Model
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class Visitor extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_ip_address;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_city;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_country;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_os;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_browser;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_network;

}
