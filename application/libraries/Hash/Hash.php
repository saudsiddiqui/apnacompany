<?php

/**
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */

namespace Hash;

class Hash {

	public static function sha512($data) {
		return hash("sha512", $data);
	}

	public static function sha256($data){
		return hash("sha256", $data);
	}

	public static function sha1($data) {
		return hash("sha1", $data);
	}

	public function md5($data) {
		return md5($data);
	}

}
