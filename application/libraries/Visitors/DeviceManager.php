<?php

namespace Visitors;

class DeviceManager
{
    public $user_agent;

    public function __construct()
    {
        $this->user_agent = $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * Get the operating system of the device of the user
     * @return [type] [description]
     */
    public function getUserOperatingSystem()
    {
        $operating_system_patterns = [
            '/windows nt 10/i'     =>  'Windows 10',
            '/windows nt 6.3/i'     =>  'Windows 8.1',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 6.0/i'     =>  'Windows Vista',
            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/windows nt 5.0/i'     =>  'Windows 2000',
            '/windows me/i'         =>  'Windows ME',
            '/win98/i'              =>  'Windows 98',
            '/win95/i'              =>  'Windows 95',
            '/win16/i'              =>  'Windows 3.11',
            '/macintosh|mac os x/i' =>  'Mac OS X',
            '/mac_powerpc/i'        =>  'Mac OS 9',
            '/linux/i'              =>  'Linux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/ipod/i'               =>  'iPod',
            '/ipad/i'               =>  'iPad',
            '/android/i'            =>  'Android',
            '/blackberry/i'         =>  'BlackBerry',
            '/webos/i'              =>  'Mobile'
        ];
        return $this->deviceRegexParser($operating_system_patterns);
    }

    /**
     * Get the browser used by user to visit the site
     * @return string browser name
     */
    public function getUserBrowser()
    {
        $browser_patterns = [
            '/msie/i'       =>  'Internet Explorer',
            '/firefox/i'    =>  'Firefox',
            '/safari/i'     =>  'Safari',
            '/chrome/i'     =>  'Chrome',
            '/edge/i'       =>  'Edge',
            '/opera/i'      =>  'Opera',
            '/netscape/i'   =>  'Netscape',
            '/maxthon/i'    =>  'Maxthon',
            '/konqueror/i'  =>  'Konqueror',
            '/mobile/i'     =>  'Handheld Browser'
        ];
        return $this->deviceRegexParser($browser_patterns);
    }

    protected function deviceRegexParser($device_pattern)
    {
        $device_detail = "";
        foreach ($device_pattern as $single_regex_pattern => $regex_value) {
            if (preg_match($single_regex_pattern, $this->user_agent)) {
                $device_detail = $regex_value;
            }
        }
        return $device_detail;
    }
}
