<?php

/**
 * The Vendor Model
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class Vendor extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_user_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_shop_name;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_shop_address;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_joining_date;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_registered;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_kyc_completed;

   

}
