<?php

/**
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class CompanyEmployee extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 25
     * 
     */
    protected $_user_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_employee_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_department_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_joining_date;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_role;

}
