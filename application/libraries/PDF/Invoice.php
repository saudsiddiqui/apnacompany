<?php
/**
 * sample class for using pdf
 */
namespace PDF;

require_once(dirname(__FILE__)."/lib/fpdf.php");

class Invoice
{
    private $author = "Sales Department";
    private $creator = "Omanzo";
    private $title = "Order Invoice";
    private $subject = "A subject for PDF";

    public $fpdf;

    public $tax_rate = 14.5;
    /**
     * Creating a new Page
     * @var P: Portrait
     * @var pt: point
     * @var A5: Paper size
     */
    public function __construct()
    {
        $this->fpdf = new \FPDF("P", "pt", "A5");
        $this->pageSettings();
        $this->companySetting();
        
    }

    public function pageSettings()
    {
        $this->fpdf->AddPage();
        $this->fpdf->SetAuthor($this->author);
        $this->fpdf->SetCreator($this->creator);
        $this->fpdf->SetTitle($this->title);
        $this->fpdf->SetSubject($this->subject);
        $this->fpdf->Image(dirname(__FILE__)."/logo.png", 30, 30);
    }
    public function pageContent($content = false)
    {

    }

    public function generate()
    {
        return $this->fpdf->Output();
    }

    public function companySetting()
    {
        $this->fpdf->SetFont('Helvetica', 'BI', 15);
        $this->fpdf->Cell(260);
        $this->fpdf->Cell(100, 20, 'Omanzo Pvt. Ltd.', 0, 0, 'C');
        $this->fpdf->SetFont('Helvetica', '', 10);
        $this->fpdf->Ln(15);
        $this->fpdf->Cell(200);
        $this->fpdf->Cell(40, 20, 'Address :', 0, 0, 'R');
        $this->fpdf->SetFont('Courier', '', 10);
        $this->fpdf->Cell(90, 20, 'D 652, DDA Flats Jaosola', 0, 0, 'L');
        $this->fpdf->Ln(12);
        $this->fpdf->Cell(240);
        $this->fpdf->Cell(90, 20, 'Jamia Nagar, New Delhi', 0, 0, 'L');
        $this->fpdf->Ln(12);
        $this->fpdf->Cell(240);
        $this->fpdf->Cell(90, 20, 'PIN - 110025', 0, 0, 'L');
        $this->fpdf->Ln(12);
        $this->fpdf->Cell(200);
        $this->fpdf->SetFont('Helvetica', '', 10);
        $this->fpdf->Cell(40, 20, 'Email :', 0, 0, 'R');
        $this->fpdf->SetFont('Courier', '', 10);
        $this->fpdf->Cell(90, 20, 'support@omanzo.com', 0, 0, 'L');
        $this->fpdf->Ln(12);
        $this->fpdf->Cell(200);
        $this->fpdf->SetFont('Helvetica', '', 10);
        $this->fpdf->Cell(40, 20, 'Website :', 0, 0, 'R');
        $this->fpdf->SetFont('Courier', 'I', 10);
        $this->fpdf->Cell(90, 20, 'https://www.omanzo.com', 0, 0, 'L');
    }

    public function pageBreak()
    {
        $y = $this->fpdf->GetY()+30;
        $this->fpdf->Line(30, $y, 390, $y);
        $this->fpdf->SetFont('Arial', 'B', 12);
        $this->fpdf->Ln(35);
        $this->fpdf->Cell(135);
        $this->fpdf->Cell(100, 20, 'Order Invoice', 'B', 0, 'C');
    }

    public function billingInfo($billing_datils)
    {
        $this->pageBreak();
        $this->fpdf->SetFont('Arial', 'IB', 12);
        $this->fpdf->Ln(25);
        $this->fpdf->Cell(15);
        $this->fpdf->Cell(10, 10, 'To :', '0', 0, 'C');
        /**
         * Name
         */
        $this->fpdf->SetFont('Courier', 'IB', 10);
        $this->fpdf->Ln(15);
        $this->fpdf->Cell(15);
        $this->fpdf->Cell(90, 10, $billing_datils['name'], '0', 0, 'L');
        $this->fpdf->SetFont('Times', '', 10);
        $this->fpdf->Ln(15);
        $this->fpdf->Cell(15);
        $this->fpdf->Cell(90, 10, "Email : ".$billing_datils['email'], '0', 0, 'L');
        $this->fpdf->Ln(15);
        $this->fpdf->Cell(15);
        $this->fpdf->Cell(90, 10, "Contact : ".$billing_datils['contact'], '0', 0, 'L');
        /**
         * Address
         */
        $this->fpdf->SetFont('Times', '', 10);
        foreach ($billing_datils["address"] as $address_line) {
            $this->fpdf->Ln(15);
            $this->fpdf->Cell(15);
            $this->fpdf->Cell(90, 10, $address_line, '0', 0, 'L');
        }

        ##----------------------------Right Side -----------------------------------------#
        $y = $this->fpdf->GetY();
        $this->fpdf->SetY($y-90);
        /**
         * Invoice Number
         */
        $this->fpdf->SetFont('Times', 'B', 10);
        $this->fpdf->Cell(220);
        $this->fpdf->Cell(40, 10, "Invoice Number : ", '0', 0, 'L');
        $this->fpdf->SetFont('Courier', 'B', 10);
        $this->fpdf->Cell(100, 10, $billing_datils['invoice_number'], '0', 0, 'R');
        /**
         * Invoice Date
         */
        $this->fpdf->Ln(15);
        $this->fpdf->SetFont('Times', 'B', 10);
        $this->fpdf->Cell(220);
        $this->fpdf->Cell(40, 10, "Invoice Date : ", '0', 0, 'L');
        $this->fpdf->SetFont('Courier', 'B', 10);
        $this->fpdf->Cell(100, 10, $billing_datils['invoice_date'], '0', 0, 'R');
        /**
         * BarCode For Invoice Number
         */
        $BarCode = "http://api-bwipjs.rhcloud.com/?bcid=code39&scale=0.5&text=".$billing_datils['invoice_number']."&includetext&image.png";
        $this->fpdf->Ln();
        $this->fpdf->Image($BarCode, $this->fpdf->GetX()+220, $this->fpdf->GetY()+10);
        #----------------------------------------------------------------------------------#
    }

    public function productList($products = false)
    {
        $y = $this->fpdf->GetY();
        $this->fpdf->SetY($y+70);
        $this->fpdf->SetFont('Times', '', 10);
        $this->fpdf->Ln(25);
        $this->fpdf->Cell(15);
        $this->fpdf->Cell(30, 15, "S.NO", 1, 0, 'C');
        $this->fpdf->Cell(180, 15, "Product", 1, 0, 'C');
        $this->fpdf->Cell(30, 15, "Qty.", 1, 0, 'C');
        $this->fpdf->Cell(50, 15, "Price", 1, 0, 'C');
        $this->fpdf->Cell(60, 15, "Total", 1, 0, 'C');
        $this->fpdf->Ln();
        if ($products) {
            $total_price = 0;
            for ($i=0; $i<count($products); $i++) {
                $this->fpdf->Cell(15);
                $this->fpdf->Cell(30, 16, $i+1, 1, 0, 'C');
                $this->fpdf->Cell(180, 16, $products[$i]["name"], 1, 0, 'L');
                $this->fpdf->Cell(30, 16, number_format($products[$i]["quantity"]), 1, 0, 'R');
                $this->fpdf->Cell(50, 16, number_format($products[$i]["price"]), 1, 0, 'R');
                $this->fpdf->Cell(60, 16, number_format((int)$products[$i]["price"]*(int)$products[$i]["quantity"]), 1, 0, 'R');
                $this->fpdf->Ln();
                $total_price += (int)$products[$i]["price"]*(int)$products[$i]["quantity"];
            }
            $this->fpdf->Cell(225);
            $this->fpdf->Cell(80, 16, "Sub Total", 1, 0, 'R');
            $this->fpdf->Cell(60, 16, number_format($total_price), 1, 0, 'R');
            $this->fpdf->Ln();
            $tax = ($this->tax_rate*$total_price)/100;
            $this->fpdf->Cell(225);
            $this->fpdf->Cell(80, 16, "Tax @ ".$this->tax_rate."%", 1, 0, 'R');
            $this->fpdf->Cell(60, 16, number_format($tax, 2), 1, 0, 'R');
            $this->fpdf->Ln();
            $this->fpdf->Cell(225);
            $this->fpdf->Cell(80, 16, "Payble Amount ", 1, 0, 'R');
            $this->fpdf->Cell(60, 16, number_format(round($tax+$total_price)), 1, 0, 'R');
        }
        $this->footer();
    }

    public function footer()
    {
        $this->fpdf->SetY(-100);
        $this->fpdf->SetFont('Arial', 'I', 10);
        $this->fpdf->Cell(80);
        $this->fpdf->Cell(200, 10, "********* Thankyou for shopping with Us *********", 0, 0, 'C');
        $this->fpdf->Ln();
        $this->fpdf->Cell(20);
        $this->fpdf->Cell(20, 10, "Note**", 0, 0, 'L');
        $this->fpdf->Ln();
        $this->fpdf->Cell(40);
        $this->fpdf->Cell(200, 10, "1. This is a computer genrated bill and does not require any signature.", 0, 0, 'L');
        $this->fpdf->Ln();
        $this->fpdf->Cell(40);
        $this->fpdf->Cell(200, 10, "2. If you find any mistake please contact Omanzo sales team.", 0, 0, 'L');
    }
}
