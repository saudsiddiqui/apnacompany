<?php 

/**
 * Payment class for payumoney Integration
 */
namespace Payment;
use \Framework\Registry;
class Payumoney {

	/**
	 * Url Provided by paymeny for request
	 * @var string
	 */
	private $url = "https://test.payu.in/_payment";
	/**
	 * Salt for your merchant
	 * @var string
	 */
	private $salt;
	/**	
	 * API key for payment
	 * @var sting
	 */
	private $key;
	/**	
	 * When payment is successfull
	 * @var string
	 */
	private $success_url = "http://www.omanzo.com/order/success";
	/**
	 * Url when payment is failed
	 * @var string
	 */
	private $failure_url = "http://www.omanzo.com/order/failed";
	/**
	 * Product List for Billing
	 * @var array
	 */
	public $products = [];
	/**
	 * txnid as per peumoney, random genrated string for refrence
	 * @var string
	 */
	private $txnid;

	/**
	 * Total payble amount by the user
	 * @var float
	 */
	public $amount;

	/**	
	 * This method will get api and  salt from your configuration file
	 */
	public function __construct(){
		$configuration = Registry::get("configuration");
		$configuration = $configuration->initialize();
		$parsed = $configuration->parse("configuration/payment");
		$this->salt = $parsed->payumoney->salt;
		$this->key  = $parsed->payumoney->key;
		if(!$this->salt || !$this->key){
			throw new \Exception("Payumoney salt or key not found", 1);
		}else {
			$this->txnid = $this->generateTxnId();
		}
	}

	public function totalPaybleAmount($amount){
		$this->amount = $amount;
	}

	/**
	 * list of product purchased by user for billing purpose 
	 * @param [type] $product_details [description]
	 */
	public function addProcuctForBilling($product_list) 
	{
		$add_params = [
			"isRequired"=>"true",
			"settlementEvent" =>"EmailConfirmation"
			];
		foreach($product_list as $product) {
			$product = array_merge($product, $add_params);
			$this->products["paymentParts"][] = $product;
		}

		$payment_identifier = [
				[
					"field" => "CompletionDate",
					"value" => date("d/m/Y")
				],
				[
					"field" => "TxnId",
					"value" => $this->txnid
				]
			];
		$this->products["paymentIdentifiers"][] =  $payment_identifier;
	}

	public function pay()
	{
		$billing_data = [
			"key" => $this->key,
			"txnid" => $this->txnid,
			"amount" => $this->amount,
			"productinfo" => "payment done by p",
			"firstname" => "Meraj Ahmad Siddiqui",
			"email" => "merajsiddiqui@outlook.com",
			"surl" => $this->success_url,
			"furl" => $this->failure_url,
		];
		$hash_data = $this->buildPaymentDetail($billing_data);
		$hash_data = strtolower(hash("sha512", $hash_data.$this->salt));
		$billing_data = array_merge($billing_data, ["hash" => $hash_data, "service_provider" => "payu_paisa"]);
		/**
		 * Genrate a post request temporary , find solution
		 */
		$ch = curl_init();
		$curl_options = [
			CURLOPT_URL => $this->url,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $billing_data,
 		];
 		curl_setopt_array($ch, $curl_options);
 		$process = curl_exec($ch);
 		curl_close($ch);
		

	}

	/**
	* hash sequence is the order in which data to be hashed
	* @var string
	* @param   $payumoney_hash_sequence		
	*
	* hashed data to be sent to payumoney.
	* @var string
	* @param  $hash_data_to_send
	* */
	private function buildPaymentDetail($billing_data)
	{	
		$payumoney_hash_sequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
		$payumoney_hash_sequence_array = explode("|", $payumoney_hash_sequence);
		$hash_data_to_send = "";
		foreach($payumoney_hash_sequence_array as $hash_key) {
			$hash_data_to_send .= isset($billing_data[$hash_key]) ?:"";
			$hash_data_to_send .= "|";
		}
		return $hash_data_to_send;
	}
	/**
	 * Method to genrate txn_id for payment
	 * @return string txnid
	 */
	private function generateTxnId()
	{
		return "meraj";
	}
}