<?php

/**
 * The TicketReply Model
 *
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class TicketReply extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_employee_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 10
     * 
     */
    protected $_ticket_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 150
     * 
     */
    protected $_description;


    /**
     * @column
     * @readwrite
     * @type text
     * @length 100
     * 
     */
    protected $_status;

}
