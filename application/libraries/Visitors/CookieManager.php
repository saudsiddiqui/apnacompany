<?php

namespace Visitors;

class CookieManager
{

    public static function getCookies($cookie_name)
    {
        return isset($_COOKIE[$cookie_name]) ? $_COOKIE[$cookie_name] : false;
    }

    /**
     * This method will set cookies in the Users system
     * @param array $cookie_set cookies name and cookies value
     */
    public static function setCookies($cookie)
    {
        $extra_cookie_options = [
            "path" => "/",
            "domain" => "omanzo.com",
            "secure" => false,
            "httponly" => true
        ];
        $cookie  = array_merge($cookie, $extra_cookie_options);
        setcookie(
            $cookie['name'],
            $cookie['value'],
            $cookie['expire'],
            $cookie['path'],
            $cookie['domain'],
            $cookie['secure'],
            $cookie['httponly']
        );
    }
}
