<?php

/**
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
namespace Generator;

class Unique {

	/**
	 * All digits used to create some code
	 * @var string
	 */	
	private static $numbers = "0123456789";
	/**
	 * English Alphabets in lowerCase
	 * @var string
	 */
	private static $lower_case_alphabets = "abcdefghijklmnopqrstuvwxyz";
	/**
	 * English Alphabets in Upper Case
	 * @var string
	 */
	private static $upper_case_alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	/**
	 * Special characters allowed 
	 * @var string
	 */
	private static $special_charset = "@#!)(";
	/**
	 * Referal code generator a random string
	 * @param  integer $length optional length of referal code
	 * @return string          generated referal code
	 */
	public static function referalCode($length = 6) {
		$code_string = self::$numbers;
		$referal_code = self::randomize($code_string, $length);
		return $referal_code;
	}
	/**
	 * Metod to generate a random password
	 * @param  integer $length length of the desired password
	 * @return string          random password
	 */
	public static function password($length=8) {
		$password_string = self::$numbers;
		$password_string .= self::$lower_case_alphabets;
		$password_string .= self::$upper_case_alphabets;
		$password_string .= self::$special_charset;
		$random_password = self::randomize($password_string, $length);
		return $random_password;
	}

	public static function accessToken($hash_method = "sha512"){
		$access_token_string = self::$numbers;
		$access_token_string .= self::$lower_case_alphabets;
		$access_token_string .= self::$upper_case_alphabets;
		$access_token_string .= self::$special_charset;
		$random_access_token_data = self::randomize($access_token_string, 10);
		$access_token = hash($hash_method, $random_access_token_data);
		return $access_token;
	}
	/**
	 * This function randomize an string of length n
	 * @param  string $allowed_string strings to be used in random 
	 * @param  integer $length         length of desired random string
	 * @return string $random_string	generated random string
	 */
	protected static function randomize($allowed_string, $length) {
		$random_string = "";
		$string_length = strlen($allowed_string)-1;
		for($i =0; $i< $length; $i++){
			$random_string .= $allowed_string[rand(0,$string_length)];
		}
		return $random_string;
	}
}