<?php

namespace Visitors;

class Visitor
{
    /**
     * This method will test if the cookies are enabled or ot in users browser;
     */
    public function __construct()
    {
        $test_cookie = [
            "name" => "website",
            "value" => "omanzo.com",
            "expire" => time()+100,
        ];
        CookieManager::setCookies($test_cookie);
    }

    /**
     * This method will use the class and find the detail of the user
     * @return array user details set
     */
    public function getInfo()
    {
        if (!CookieManager::getCookies("user_type")) {
            $this->newUser();
            $ip = new IPManager();
            $ip_details = $ip->getUserIPLocation($ip->getUserIPAddress());
            $ip_details = json_decode($ip_details, true);
            unset($ip);
            $device = new DeviceManager();
            $os = $device->getUserOperatingSystem();
            $browser = $device->getUserBrowser();
            unset($device);
            $info =  [
                "ip_address" => $ip_details["ip"],
                "city" => $ip_details["city"],
                "country" => $ip_details["country"],
                "os" => $os,
                "browser" => $browser,
                "internet_org" => $ip_details["org"],
            ];
            return $info;
        } else {
            return false;
        }
    }

    public function newUser()
    {
        $expiration_time  = time()+(60*60*24*365); //set for a year
        $user_cookie_data = [
            "name" => "user_type",
            "value" => "return_user",
            "expire" => $expiration_time,
        ];
        CookieManager::setCookies($user_cookie_data);
    }
}
