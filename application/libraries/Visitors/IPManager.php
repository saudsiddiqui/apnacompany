<?php

namespace Visitors;

class IPManager
{
    /**
     * Ip address of the visitor
     * @var string
     */
    public $user_ip_address;

    /**
     * https://www.ipinfo.io/
     * Max 1000 request per day (24 hours)
     * The Api URL to get address of IP ..
     * @var string
     */
    protected $api_url = "http://www.ipinfo.io";

    /**
     * Get user Ip ddress
     * @return sring Ip address of the current system
     */
    public function getUserIPAddress()
    {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }

    /**
     * This method will return the geographical location of User by some api
     * @return array array containg country, city, etc
     */
    
    public function getUserIPLocation($ip_address)
    {
        $ch = curl_init();
        $request_headers = [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8;',
            'Accept-Encoding: gzip, deflate',
            "Connection: keep-alive",
            "Content-Type: text/html; charset=UTF-8"
        ];
        $user_agent="Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:47.0) Gecko/20100101 Firefox/47.0";
        $options = [
            CURLOPT_URL => $this->api_url."/".$ip_address."/json",
            CURLOPT_CONNECTTIMEOUT => 90,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_USERAGENT => $user_agent,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_AUTOREFERER => true,
            CURLOPT_COOKIESESSION => true,
            CURLOPT_FILETIME => true,
            CURLOPT_FRESH_CONNECT => true,
            CURLOPT_HTTPHEADER => $request_headers,
            CURLOPT_COOKIESESSION => true,
            CURLOPT_ENCODING => "gzip, deflate, scdh",
        ];

        curl_setopt_array($ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
