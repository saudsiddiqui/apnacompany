<?php

/**
 * @author Meraj Ahmad Siddiqui <merajsiddiqui@outlook.com>
 */
class BankAccount extends Shared\Model {

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * 
     */
    protected $_user_id;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 255
     * @index
     * 
     */
    protected $_payee_name;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_account_number;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_ifsc_code;

    /**
     * @column
     * @readwrite
     * @type text
     * @length 15
     * 
     */
    protected $_pan_number;


}
