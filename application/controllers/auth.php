<?php
/**
* Auth COntroller to auuthenticate the user
*
* @author 
*/

use Shared\Controller;
use Framework\Registry;
use Framework\RequestMethods;
use Hash\Hash;
use Generator\Unique;
use Visitors\DeviceManager;
use SendGrid\Mailer;
use Defuse\Crypto\Crypto;

class Auth extends Controller {

    private $salt = "8e71fabad";
    /**
    * @before changeLayout
    */
    public function login(){
        $this->seo(array("title" => "Login to Admin Panel", "keywords" => "admin", "description" => "admin", "view" => $this->getLayoutView()));
        /**
        * Admin Login
        */
        
        $view = $this->getActionView();
        if(RequestMethods::post("login")=="_self"){
          $user  = \User::first([
            "email =?" => RequestMethods::post("email"),
            "password =?" => Hash::sha512(RequestMethods::post("password")),
          ]);
       
          if($user){
            $this->setUser($user);
            self::redirect("/home");
          }else{
            $view->set("err_msg","Login credentials cold not be authenticated");
          }
        }
    }

    /**
     * logout
     */
    public function logout(){
        $this->setUser(false);
        self::redirect("/login");
    }

    /**
     * @before changeLayout
     */
    public function forgotpassword(){
        $this->seo(array("title" => "Forgot Password", "keywords" => "admin", "description" => "admin", "view" => $this->getLayoutView()));
        $view = $this->getActionView();
        if(RequestMethods::post("forgotpass")=="true"){
            $access_token = Unique::accessToken();
            $user = \User::first(["email =?" => RequestMethods::post("email")]);
            $user->access_token = $access_token;
            $user->access_expiry = time()+1800;
            $user->save();
            $device = new DeviceManager();

            $user_info = [
                "name" => $user->name,
                "email" => $user->email
            ];
            $encrypted_email = Crypto::encryptWithPassword($user->email,$this->salt);
            $email_info = [
                "name" => $user->name,
                "action_url" => "http://admin.omanzo.com/auth/resetPassword/".$encrypted_email."/$access_token",
                "operating_system" =>$device->getUserOperatingSystem(),
                "browser_name" => $device->getUserBrowser()
            ];
            $this->forgotMail($user_info, $email_info);
            $view->set("err_msg","Reset Link sent to Email ID");
        }
    }
    /**
     * @before changeLayout
     */
    public function resetPassword($email, $access_token){
        $this->seo(array("title" => "Forgot Password", "keywords" => "admin", "description" => "admin", "view" => $this->getLayoutView()));
          $view = $this->getActionView();
          $view->set("email", $email);
          $view->set("token", $access_token);
          if(RequestMethods::post("reset")=="_mail"){
              $user = \User::first([
                  "email =?" => Crypto::decryptWithPassword(RequestMethods::post("_email"), $this->salt),
                  "access_token =?" => RequestMethods::post("_token"),
                  "user_type =?" => "admin",
                  "live =?" => 1
                ]);
              if($user->access_expiry > time()) {
                $user->password = Hash::sha512(RequestMethods::post("password"));
                $user->access_token = Null;
                $user->access_expiry=0;
                $user->save();
                $view->set("err_msg", "Your pasword has been rset, login with new password");
              }
              else{
                $view->set("err_msg","Token has been altered or expired");
              }
              
          }
       
    }

    public function changeLayout() {
        $this->layout = "layouts/login";
    }


    protected function forgotMail($user_info, $email_info){
        $template = APP_PATH."/application/views/layouts/mails/EmployeePasswordReset.html";
        $mail_body = file_get_contents($template);
        $replace = [];
        $search = [];
        foreach ($email_info as $key => $value) {
            $search[]  = "{{".$key."}}";
            $replace[] = $value;
        }
        $formated_mail_body = str_replace($search, $replace, $mail_body);
         $from = [
            "name" => "Omanzo Pvt. Ltd.",
            "email" => "reply@omanzo.com"
        ];
        $to = [
            ["name" => $user_info['name'], "email" => $user_info['email']],
        ];
        $subject = "Password Reset Reqested";
        $sendgrid_mailer = new Mailer();
        $sendgrid_mailer->addSender($from);
        $sendgrid_mailer->addRecipient($to);
        $sendgrid_mailer->addSubject($subject);
        $sendgrid_mailer->addBodyContent($formated_mail_body);
        $sendgrid_mailer->addReplyTo($from);
        $response = $sendgrid_mailer->send();
    }
}